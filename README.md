# Autonomous Greenhouse 

## About Sensors 
### Tekelek --> Tek766 for LoRaWAN
See [for more information](/Aquaponie/Documentation_Capteur/2018-06-07%20TEK%20766%20User%20Manual-%20V1.3.pdf)

This sensor is a technology to measure the liquid level and then communicates thsi data to a LoRaWAN network. 

- In low-power for the majority of time
- 2 major parameters
    - Scheduler TX period which is the interval in hours between LoRa radio transmission of the data (the buffer is not cleared on transmission)
    - Logging interval = interval for storing 4 measurements
    - **[Warning] logging interval < Scheduler TX period**
    - default operation : 
        - 4 transmissions sent every 24 hours, one every 6 hours. The 4th transmission is an Acknowlegde from the server
- It is possible to force a connection by pressing the button for one second
- What contains a logging data : 4 measurements 
    - ullage reported in cm 
    - sonic result code 
    - sonic RSSI 
    - inetrnal PCB temperature in °C

### LAIRD --> Sentrius RS1xx Series 
See [for more information](http://assets.lairdtech.com/home/brandworld/files/Sentrius%20RS1xx%20User%20Guide%20v1_0.pdf)

This technology offers a robust LoRa solutions to secure our end-to-end private LoRaWAN network

- It is composed of temperature and humidity sensor 
- LoRaWAN possible frequency : 868, 915 or 923MHz
- Temperature : 
    - accuracy : -10°C to +85°C 
    - range : [-40;125]
- Humidity : 
    - accuracy : 0-90% RH 
    - range : 90-100% RH 
- Battery : 2xAA replacable
- configuration with mobile application 
- storage : 10000 measurement( 256k of flash memory available)

### X-Nucleo-IKS01A2

It is an expansion board for STM32 Nucleo. 
It is equiped with : 
- Arduino Uno R3 connector
- HTS221 Humidity and temperature sensors

### MCF88 
See [for more information](/Aquaponie/Documentation_Capteur/Catalogue%20mcf88.pdf)

This technology is used for environmental monitoring purpuse. It exists a large range of models. 
In our case, an agriculture purpose, we need to use an MCF88 outdoor LoRaWAN sensor. It allows : 
- reading temperature, humidity and pressure
- sending collected dzta over the LoRaWAN network